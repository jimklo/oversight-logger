#!/bin/bash

TGT_APP=${1:-target/universal2-apple-darwin/release/oversightlogger}
ARCHIVE=${2:-oversight-logger.tar.gz}

WORKDIR=/tmp/oversight

if [ -f "${PROJ_DIR}/${ARCHIVE}" ]; then
  rm -f "${PROJ_DIR}/${ARCHIVE}"
fi

mkdir -p ${WORKDIR}

cp -av "${TGT_APP}" ${WORKDIR}/
cp -av LICENSE.md ${WORKDIR}/
cp -av README.md ${WORKDIR}/

PROJ_DIR=$(pwd)
cd ${WORKDIR} || printf 'Unable to cd to %s' "$WORKDIR"

tar -czvf "${PROJ_DIR}/${ARCHIVE}" ./*