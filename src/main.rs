use std::{env, fs};
use std::collections::HashSet;
use std::path::Path;
use std::process::exit;
use std::time::Duration;

use chrono::{DateTime, Local};
use serde::Serialize;

use crate::archive::RotatingLogger;
use crate::process::{ProcessChain, ProcessInfo};

mod archive;
mod process;

const VERSION: Option<&str> = option_env!("CARGO_PKG_VERSION");
const DEFAULT_VERSION: &str = "0.0.0";


#[derive(Serialize, Debug)]
struct ProcessEvent {
    timestamp: DateTime<Local>,
    device: String,
    event: String,

    #[serde(skip_serializing_if="Option::is_none")]
    process: Option<ProcessChain>,
}

#[derive(Serialize, Debug)]
struct ErrMsg {
    timestamp: DateTime<Local>,
    err: String
}

impl Default for ErrMsg {
    fn default() -> Self {
        ErrMsg {
            timestamp: Local::now(),
            err: Default::default()
        }
    }
}

impl Default for ProcessEvent {
    fn default() -> Self {
        ProcessEvent {
            timestamp: Local::now(),
            device: Default::default(),
            event: Default::default(),
            process: None
        }
    }
}

struct Oversight<'a> {
    err_log: RotatingLogger,
    pub log: RotatingLogger,
    options: OversightOpts<'a>
}

#[derive(Clone)]
struct OversightOpts<'a> {
    log_dir: &'a str,
    num_bak: usize,
    rollover: Duration,
}

impl<'a> Default for OversightOpts<'a> {
    fn default() -> Self {
        OversightOpts { log_dir: ".oversight", num_bak: 7, rollover: Duration::from_secs(60 * 60 * 24) }
    }
}

impl<'a> Oversight<'a> {
    #[allow(dead_code)]
    pub fn new() -> Self {
        Oversight::new_with_opts(&OversightOpts::default())
    }

    #[allow(dead_code)]
    pub fn new_with_log_dir(log_dir: &'a str) -> Self {
        Oversight::new_with_opts(&OversightOpts { log_dir, ..OversightOpts::default()})
    }

    pub fn new_with_opts(opts: &OversightOpts<'a>) -> Self {
        Oversight {
            err_log: RotatingLogger::new_with_options(format!("{}/oversight_error_log.jsonl", opts.log_dir).as_str(), &opts.rollover, opts.num_bak),
            log: RotatingLogger::new_with_options(format!("{}/oversight_activity_log.jsonl", opts.log_dir).as_str(), &opts.rollover, opts.num_bak),
            options: opts.clone()
        }
    }

    fn purge(&self) -> Result<(),()> {
        let mut dirs: HashSet<&Path> = HashSet::new();

        if let Some(root) = self.err_log.log_file.parent() {
            dirs.insert(root);
        }

        if let Some(root) = self.log.log_file.parent() {
            dirs.insert(root);
        }

        let mut success = true;
        for p in dirs {
            if let Err(e) = fs::remove_dir_all(&p) {
                eprintln!("Error: problem removing directory: {:?} = {:?}", &p, e);
                success = false;
            }
        }

        match success {
            true => { Ok(()) }
            false => { Err(()) }
        }
    }

    fn show_usage(&self) {
        let args: Vec<String> = env::args().collect();

        let app_name = match args.get(0) {
            Some(arg0) => {
                if let Some(slash_idx) = arg0.rfind('/') {
                    let mut app_path = arg0.clone();
                    app_path.replace_range(..=slash_idx, "");
                    app_path
                } else {
                    arg0.clone()
                }
            },
            None => "oversightlogger".to_string()
        };

        println!("Usage:");
        println!("\t{} -device <camera|microphone> -event <on|of> [-process <pid>] [-purge]\n\n", app_name);
        println!("\t-device DEVICE   The device that was activated. Either 'camera' or 'microphone'.");
        println!("\t-event STATE     The state of the device. Either 'on' or 'off'.");
        println!("\t-process PID     The process id of the application interacting with the device.");
        println!("\t-purge           Delete the log directory and exit {}", &self.options.log_dir);
        println!("\t-version         Print the version and exit. {}", VERSION.unwrap_or(DEFAULT_VERSION));
        println!("\t-help            Print this help.");

    }

    fn get_process(&self, args: &Vec<String>) -> Option<ProcessEvent> {
        let mut process: ProcessEvent = Default::default();

        let num_args = args.len();
        if num_args > 1 {
            let mut idx = 1;
            while idx < num_args {
                if let Some(arg) = args.get(idx) {
                    match arg.as_str() {
                        "-version" => {
                            println!("{}", VERSION.unwrap_or(DEFAULT_VERSION));
                            exit(0);
                        },
                        "-purge" => {
                            self.purge().unwrap();
                            println!("Removed log directory: {}", &self.options.log_dir);
                            exit(0);
                        },
                        "-device" => {
                            process.device = args.get(idx + 1)?.to_string();
                            idx += 1
                        },
                        "-event" => {
                            process.event = args.get(idx + 1)?.to_string();
                            idx += 1
                        },
                        "-process" => {
                            process.process = match args.get(idx + 1)?.parse::<usize>() {
                                Ok(pid) => { Some(ProcessChain{ pid, ..ProcessChain::default()}) }
                                Err(e) => {
                                    let err = ErrMsg {
                                        err: format!("Parse PID error: {}", e.to_string()),
                                        ..ErrMsg::default()
                                    };
                                    self.err_log.write_log(&err).unwrap();
                                    None
                                }
                            };

                            if let Some(chain) = process.process {
                                let sys = ProcessInfo::new();
                                process.process = sys.get_process_chain(&chain.pid)
                            }
                            idx += 1
                        },
                        "-help" => {
                            self.show_usage();
                            exit(0);
                        }
                        unknown_arg => {
                            let err = ErrMsg {
                                err: format!("Unknown argument: {}", unknown_arg),
                                ..ErrMsg::default()
                            };
                            self.err_log.write_log(&err).unwrap();
                            println!("ERROR: Unknown argument: {}\n", unknown_arg);
                            self.show_usage();
                            exit(0);
                        }
                    }
                }
                idx += 1;
            }
            Some(process)
        } else {
            self.show_usage();
            None
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let osight = Oversight::new();
    if let Some(p) = osight.get_process(&args) {
        osight.log.write_log(&p).unwrap();
    }

}

#[cfg(test)]
mod tests {
    use sysinfo::{ProcessExt, System, SystemExt};

    use crate::Oversight;

    #[test]
    fn test_write_log() {
        let sys = System::new_all();


        let o = Oversight::new_with_log_dir(".test");
        o.purge().expect("Should Purge");
        let all_args: Vec<Vec<String>> = vec![
            vec!["", "-device", "camera", "-event", "on", "-process" ].iter().map(|s| s.to_string()).collect(),
            vec!["", "-device", "microphone", "-event", "on", "-process" ].iter().map(|s| s.to_string()).collect(),
            vec!["", "-device", "camera", "-event", "off" ].iter().map(|s| s.to_string()).collect(),
            vec!["", "-device", "microphone", "-event", "off" ].iter().map(|s| s.to_string()).collect(),
        ];

        for (pid, p) in sys.processes() {
            if p.parent().is_none() && p.exe().starts_with("/Applications/") {
                for args in &all_args {
                    let mut p_args = args.to_vec();
                    if p_args.last().unwrap().eq("-process") {
                        p_args.push(pid.to_string());
                    }
                    if let Some(p) = o.get_process(&p_args) {
                        o.log.write_log(&p).expect("Problem writing the log.");
                    }
                }
            }
        }

    }
}