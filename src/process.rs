use serde::{Deserialize, Serialize};
use sysinfo::{Pid, ProcessExt, System, SystemExt};

pub struct ProcessInfo {
    sys: System
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ProcessChain {
    pub pid: usize,
    pub name: String,
    #[serde(skip_serializing_if="Option::is_none")]
    pub child: Option<Box<ProcessChain>>
}

impl Default for ProcessChain {
    fn default() -> Self {
        ProcessChain { pid: 0, name: "Unknown".to_string(), child: None }
    }
}
impl ProcessInfo {

    pub fn new() -> Self {
        ProcessInfo { sys: System::new_all() }
    }

    pub fn get_process_chain(&self, pid: &usize) -> Option<ProcessChain> {
        if let Some(proc) = self.sys.process(Pid::from(*pid)) {
            let mut root = ProcessChain {
                pid: pid.clone(),
                name: proc.name().to_string(),
                child: None
            };

            while let Some(parent_pid) = proc.parent() {
                if parent_pid == Pid::from(root.pid) {
                    break;
                }

                if let Some(parent_proc) = self.sys.process(parent_pid) {
                    let new_root = ProcessChain {
                        pid: parent_proc.pid().into(),
                        name: parent_proc.name().to_string(),
                        child: Some(Box::new(root.clone()))
                    };

                    root = new_root;
                }
            }
            Some(root)
        } else {
            None
        }
    }

}

#[cfg(test)]
mod test {
    use sysinfo::{PidExt, ProcessRefreshKind, RefreshKind};
    use super::*;


    #[test]
    fn test_process_info() {
        let pi = ProcessInfo::new();

        let r = RefreshKind::new();
        let r = r.with_processes(ProcessRefreshKind::everything());
        let system = System::new_with_specifics(r);


        for (pid, _proc) in system.processes() {
            let u_pid = usize::try_from(pid.as_u32()).unwrap();
            let chain = pi.get_process_chain(&u_pid);

            assert!(chain.is_some());
            eprintln!("{}", serde_json::to_string(&chain.unwrap()).unwrap());
        }


    }

}