use std::fs;
use std::fs::{File, OpenOptions};
use std::path::{Path, PathBuf};
use dirs::home_dir;
use serde::Serialize;
use std::io::{BufReader, Write};
use std::time::{SystemTime, Duration};
use serde_json::Deserializer;

pub struct RotatingLogger {
    pub log_file: PathBuf,
    max_dur: Duration,
    bak_copies: usize
}

fn ensure_parent_path(file_path: &Path) {
    if file_path.parent().is_some_and(| d |!d.exists()) {
        match fs::create_dir_all(file_path.parent().unwrap() ) {
            _ => {}
        }
    }
}

fn get_path(file_name: &str) -> PathBuf {

    let file_path = match home_dir() {
        None => PathBuf::from(file_name),
        Some(p) => p.join(file_name)
    };

    file_path
}

impl RotatingLogger {
    #[allow(dead_code)]
    pub fn new(path_fragment: &str) -> RotatingLogger {
        RotatingLogger::new_with_duration( path_fragment, &Duration::from_secs(60 * 60 * 24 ) )
    }

    #[allow(dead_code)]
    pub fn new_with_duration(path_fragment: &str, max_dur: &Duration) -> Self {
        RotatingLogger::new_with_options(path_fragment, max_dur, 7)
    }

    pub fn new_with_options(path_fragment: &str, max_dur: &Duration, bak_copies: usize) -> Self {
        let log_file = get_path(path_fragment);
        ensure_parent_path(&log_file);
        RotatingLogger { log_file, max_dur: max_dur.clone(), bak_copies }
    }

    #[allow(dead_code)]
    pub fn set_duration(&mut self, new_duration:Duration) {
        self.max_dur = new_duration;
    }

    pub fn write_log<T>(&self, log_obj:&T) -> std::io::Result<()>
        where T: ?Sized + Serialize {

        ensure_parent_path(&self.log_file);

        self.rotate_logs()?;

        match serde_json::to_string(log_obj) {
            Ok(log_msg) => {
                let mut f = OpenOptions::new()
                    .write(true)
                    .append(true)
                    .create(true)
                    .open(&self.log_file)?;
                write!(&mut f, "{}\n", log_msg)?;
                f.flush()?;
            }
            _ => {}
        }
        Ok(())
    }

    #[allow(dead_code)]
    pub fn read_log<T: serde::de::DeserializeOwned>(&self, index: Option<usize>) -> Vec<T> {
        let mut entries: Vec<T> = Vec::new();

        let file_name;
        if index.is_none() {
            file_name = self.log_file.clone();
        } else {
            let index = index.unwrap();
            file_name = self.log_file.parent().unwrap().join(format!("{}.{}", self.log_file.file_name().unwrap().to_str().unwrap(), index));
        }

        if let Ok(fh) = File::open(file_name) {
            let reader = BufReader::new(fh);

            let stream = Deserializer::from_reader(reader).into_iter::<T>();
            for log_entry in stream {
                entries.push(log_entry.unwrap());
            }
        }

        entries
    }


    fn rotate_logs(&self) -> std::io::Result<()> {
        if let Ok(file_meta) = fs::metadata(self.log_file.as_path()) {
            if let Ok(create_time) = file_meta.created() {
                let cur_time = SystemTime::now();

                if cur_time.duration_since(create_time).unwrap().gt(&self.max_dur) {

                    let mut max_idx = self.bak_copies.clone();
                    while max_idx > 1 {
                        let next_name = format!("{}.{}", self.log_file.file_name().unwrap().to_str().unwrap(), max_idx);
                        let cur_name = format!("{}.{}", self.log_file.file_name().unwrap().to_str().unwrap(), max_idx-1);

                        let next_path = self.log_file.parent().unwrap().join(next_name);
                        let cur_path = self.log_file.parent().unwrap().join(cur_name);

                        // if both current and next exist, need to remove next
                        if cur_path.exists() && next_path.exists() {
                            fs::remove_file(&next_path)?;
                        }

                        // if cur exists, rename to next
                        if cur_path.exists() {
                            fs::rename(&cur_path, &next_path)?;
                        }

                        // decrement cur
                        max_idx-=1;

                        // move the current log to the backup if this is the last one.
                        if max_idx == 1 {
                            fs::rename(&self.log_file, &cur_path)?;
                        }
                    }
                }
            }
        }
        Ok(())
    }
}


#[cfg(test)]
mod tests {
    use std::fs;
    use std::ops::Add;
    use std::thread::sleep;
    use std::time::Duration;
    use chrono::{DateTime, Local};
    use serde::{Deserialize, Serialize};
    use crate::archive::{get_path, RotatingLogger};

    #[derive(Serialize, Deserialize, Debug, Default, Eq, PartialEq)]
    struct LogEntry {
        msg: String,
        ts: DateTime<Local>
    }


    #[test]
    fn test_rotate() {

        let max_age = Duration::from_secs(5);
        let sleep_time = max_age.add(Duration::from_secs(2));

        let rl = RotatingLogger::new_with_duration(".test/foo.log", &max_age);
        let mut entries: Vec<LogEntry> = Vec::new();

        for  i in 1..10 {
            let le = LogEntry{ msg: format!("This is a test #{}", i), ..Default::default() };
            rl.write_log::<LogEntry>(&le).unwrap();
            entries.push(le);
            sleep(sleep_time)
        }


        let main_log: Vec<LogEntry> = rl.read_log(None);
        assert_eq!(main_log.len(), 1);
        assert_eq!(main_log.first(), entries.last());


        for i in 1..6 {
            let bak_log: Vec<LogEntry> = rl.read_log(Some(i));
            assert_eq!(bak_log.len(), 1);
            assert_eq!(bak_log.first(), entries.get(8-i))
        }

        fs::remove_dir_all(get_path(".test")).unwrap();


    }
}