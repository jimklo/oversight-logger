# OverSight Logger


License: [MIT](LICENSE.md) | Build Info: [![pipeline status](https://gitlab.com/jimklo/oversight-logger/badges/main/pipeline.svg)](https://gitlab.com/jimklo/oversight-logger/-/commits/main) | 
[![Latest Release](https://gitlab.com/jimklo/oversight-logger/-/badges/release.svg)](https://gitlab.com/jimklo/oversight-logger/-/releases)

This is a very simple logger that integrates with [Objective See's](https://objective-see.org)
[OverSight](https://objective-see.org/products/oversight.html).

This project is intentionally a bit over-engineered.  It was used to practice designing and building Rust applications.
Hence there are some design patterns that aren't really necessary for a minimal solution and are
present for the benefit of learning the Rust language.


## What it does

Simple command line application that when executed with following arguments:

```text
oversightlogger -device <camera|microphone> -event <on|off> -process <pid>
```
It will create a timestamped log of the execution in:

`$HOME/.oversight/oversight_activity_log.jsonl`

It will rotate logs that are older than one day, and keep no more than 7 days of logs.

Errors are written to:

`$HOME/.oversight/oversight_error_log.jsonl`


## Logfile Format

OverSight Logger uses JSON Lines for logging.

A LogEntry will look like this:

```json lines
{"timestamp":"2023-09-01T14:42:34.013523-07:00","device":"camera","event":"on","process":2836,"name":"zoom.us"}
```

## Building

As this is a MacOS app, you'll need to build the binaries (until I learn how to use CI/CD to build MacOS releases)

1. Install [Rust](https://www.rust-lang.org/tools/install).
2. Download or clone this Git repository
3. Test the current release:
    ```shell
    cargo test
    ```
4. Assuming tests worked. Build a release:
    ```shell
    cargo build --release
    ```
5. Copy the built `oversightlogger` binary from `target/release` into a `bin` directory
   of your choice on your system. I like to use `~/bin/`.
6. Configure OverSight.
   - Enable the **Execute** option and browse to where you copied the `oversightlogger` binary.
   - Enable the **Pass Arguments** option so that `oversightlogger` receives the information
     that OverSight captures as devices are enabled/disabled from various applications.
   <img src="oversight_prefs.png" style="max-width:600px;">

Now whenever a device that Oversight monitors triggers an event, it will be logged to a file
that's easily reviewed.

